package org.green.kav.cmd.git;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.lib.StoredConfig;
import org.eclipse.jgit.revwalk.RevCommit;
import org.green.kav.cmd.git.service.GitManipulatorService;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.io.File;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SpringBootTest
class CmdGitManipulatorApplicationTests {
    private final static Logger logger = LoggerFactory.getLogger(CmdGitManipulatorApplicationTests.class);

    @Autowired
    private GitManipulatorService gitManipulatorService;

    @Test
    public void testGetAllGit() throws Exception {
//        gitManipulatorService.findSubGit("D:\\Users\\sm13\\Documents\\development\\ipf-clean-elastic");
//		gitManipulatorService.findSubGit("D:\\Users\\sm13\\Documents\\development\\office");
//		gitManipulatorService.findSubGit("D:\\Users\\sm13\\Documents\\development\\fauna");
		gitManipulatorService.findSubGit("D:\\Users\\sm13\\Documents\\development\\f");
//        gitManipulatorService.findSubGit("D:\\Users\\sm13\\Documents\\development\\astra-db-projects"); // there was not changes
//        gitManipulatorService.cloneDir("https://gitlab.com/public_projects/jar_manager.git", "D:\\Users\\sm13\\Documents\\development\\g");
    }

    @Test
    public void testGetBranchesByRemoteUrs() throws Exception{
        Set<String> input = new HashSet<>();
        input.add("d");
        input.add("a");
        input.add("b");
        input.add("d");
        input.add("c");
        Set<String> output = new HashSet<>(getIdentity(input));
        System.err.println(output);

        List<String> output2 = new ArrayList<>(getIdentity(input));
        System.err.println(output2);


        Instant start = Instant.now();

        Thread.sleep(10_000l);
        List<String> branches = gitManipulatorService.getBranchesByRemoteDir("https://gitlab.com/public_projects/ipf-clean-elastic.git");
        System.err.println(branches);

        System.err.println(Duration.between(start, Instant.now()).toSeconds());
    }

    public Collection<String> getIdentity(Collection<String> dataSet) {
        Collection<String> newData = new ArrayList<>();
        for (String id: dataSet) {
            newData.add(id);
        }

        return newData;
    }

    @Test
    public void testGetBranchesByLocalDir() throws Exception {
        Git git = Git.open(new File("D:\\Users\\sm13\\Documents\\development\\ipf-clean-elastic"));
        System.err.println(gitManipulatorService.getBranchesByLocalDir(git.getRepository()));
    }

    @Test
    public void testUpdateLocal() throws Exception {
        gitManipulatorService.updateLocal("D:\\Users\\sm13\\Documents\\development\\a\\ipf-clean-elastic", "main");
    }

    @Test
    public void changeBranch() throws Exception {
        Git git = Git.open(new File("D:\\Users\\sm13\\Documents\\development\\a\\ipf-clean-elastic"));
        gitManipulatorService.changeBranch(git, "bugfix");
    }

    @Test
    public void updateBranch() throws Exception {
        String projectDir = "D:\\Users\\sm13\\Documents\\development\\ipf\\be\\version-bump";
        gitManipulatorService.updateSubGit(projectDir, "develop");
    }

    @Test
    void contextLoads() throws Exception {
//        Git git = Git.open(new File("D:\\Users\\sm13\\Documents\\development\\bitbucket-web-client"));
        Git git = Git.open(new File("D:\\Users\\sm13\\Documents\\development\\ipf-clean-elastic"));
        String describe = git.describe().call();

        gitManipulatorService.getBranchesByLocalDir(git.getRepository());

        logger.info("describe: {}", describe); // null

        Status status = git.status().call();

        /**
         *  repository not found: D:\Users\sm13\Documents\development\a
         *
         *  or
         *
         * src/main/java/org/green/bitbucket/web/client/service/Main.java
         * pom.xml
         * src/main/java/org/green/bitbucket/web/client/model/response/PomResultModel.java
         * src/main/java/org/green/bitbucket/web/client/service/BitbucketClientService.java
         */
        status.getModified().forEach(s -> {logger.info("{}", s);});

        status.getConflicting().forEach(s -> {
            System.err.println("# " + s);
        });

        String gitwebDescription = git.getRepository().getGitwebDescription();

        /**
         *  Unnamed repository; edit this file 'description' to name the repository.
         */
        logger.info("web description: {}", gitwebDescription);

        Repository repository = git.getRepository();
        System.err.println(gitManipulatorService.getActiveBranch(git));

        StoredConfig storedConfig = repository.getConfig();

        storedConfig.getSections().forEach(s -> {
            System.err.println(" - " + s);
            storedConfig.getSubsections(s).stream().forEach(s1 -> {
                System.err.println("   --- " + s1);
            });
            storedConfig.getNames(s).stream().forEach(s1 -> {
                System.err.println("  -- " + s1);
            });
        });

        System.err.println(" ---- " + repository.getGitwebDescription());

        repository.getRemoteNames().stream().forEach(s -> {
            System.err.println("* " + s);
        });


//        RemoteConfig remoteCOnfig = git.remoteSetUrl().call(); // this is just giving null pointer.
//        remoteCOnfig.getPushURIs().forEach(urIish -> {
//            System.err.println("host: "+ urIish.getHost());
//        });

        git.remoteList().call().forEach(remoteConfig -> {
            remoteConfig.getURIs().forEach(urIish -> {
                System.err.println(" * urIish: " + urIish.toString()); // remote directory
            });
        });

        Iterable<RevCommit> revCommits = git.log().call();

        /**
         * a Added new project.
         *
         * a Configure SAST in `.gitlab-ci.yml`, creating this file if it does not already exist
         * a Initial commit
         */
        revCommits.forEach(revCommit -> {
            logger.info("{}: ", revCommit.getFullMessage());
            revCommit.getFooterLines().stream().forEach(footerLine -> {
                logger.info("FooterLine {}, {}, {}", footerLine.getKey(), footerLine.getValue(), footerLine.getEmailAddress());
            });
        });
    }
}
