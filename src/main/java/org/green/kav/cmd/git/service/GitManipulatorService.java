package org.green.kav.cmd.git.service;

import org.eclipse.jgit.api.CreateBranchCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PullCommand;
import org.eclipse.jgit.api.PullResult;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.JGitInternalException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.lib.StoredConfig;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.RemoteConfig;
import org.eclipse.jgit.transport.URIish;
import org.green.kav.cmd.git.controller.model.GitCloneRequestModel;
import org.green.kav.cmd.git.controller.model.GitCloneResponseModel;
import org.green.kav.cmd.git.controller.model.GitFailReport;
import org.green.kav.cmd.git.exception.GitManipulatorException;
import org.green.kav.cmd.git.service.model.GitInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.javatuples.Pair;

@Service
public class GitManipulatorService {
    private final static Logger log = LoggerFactory.getLogger(GitManipulatorService.class);

    private final static List<String> ignoreFiles = new ArrayList<>();
    private final CredentialsProvider credentialsProvider;

    static {
        ignoreFiles.add(".mvn");
        ignoreFiles.add(".git");
        ignoreFiles.add("target");
        ignoreFiles.add(".idea");
    }

    @Autowired
    public GitManipulatorService(CredentialsProvider credentialsProvider) {
        this.credentialsProvider = credentialsProvider;
    }

    public void updateBranchesWith(List<String> gitUrls, String branch) {

    }
    public GitCloneResponseModel cloneDir(List<String> gitUrls, String localDir, String branch) {
        GitCloneResponseModel responseModel = new GitCloneResponseModel();
        File parentDirectory = new File(localDir);
        if (!parentDirectory.exists()) parentDirectory.mkdirs();

        List<String> success = new ArrayList<>();
        for (String url : gitUrls) {
            Git git = null;
            try {
                String projectName = getProjectDirName(url);
                String newDir = parentDirectory.getAbsolutePath() + File.separator + projectName;
                log.info("clone dir {}", newDir);
                git = Git.cloneRepository()
                   .setCredentialsProvider(credentialsProvider)
                   .setURI(url)
                   .setBranch(branch)
                   .setCloneAllBranches(true)
                   .setDirectory(new File(newDir))
                   .call();
                success.add(url);
                responseModel.addSuccess(newDir);
            } catch (GitAPIException | MalformedURLException | JGitInternalException e) {
                log.error("failed to clone {}", url);
                responseModel.addFailed(url, e.getMessage());
            } finally {
                if (git != null) git.close();
            }
        }

        responseModel.setFailedCount(responseModel.getFailed().size());
        responseModel.setSuccessCount(success.size());
        return responseModel;
    }

    private String getProjectDirName(String url) throws MalformedURLException {
        URL remoteUrl = new URL(url);
        String fileNameWthExtension = remoteUrl.getFile();
        return remoteUrl.getFile().substring(0, fileNameWthExtension.indexOf("."));
    }

    public void cloneDir(String remoteDir, String localDir, String branch) throws GitManipulatorException {
        try {
            Git.cloneRepository().setCredentialsProvider(credentialsProvider).setURI(remoteDir).setDirectory(new File(localDir)).call();
        } catch (GitAPIException e) {
            log.error("failed to clone {}", remoteDir);
            throw new GitManipulatorException("failed to clone " + remoteDir, e);
        }
        log.info("Successfully cloned {} in {}", remoteDir, localDir);
    }

    public void changeBranch(Git git, String existingBranch) throws GitManipulatorException {

        try {
            git.checkout().setName(existingBranch).setUpstreamMode(CreateBranchCommand.SetupUpstreamMode.TRACK).call();
        } catch (GitAPIException e) {
            throw new GitManipulatorException("Change branch failed for: " + git.getRepository().getDirectory().getAbsolutePath() , e);
        }
    }

    public void updateLocal(String localDir, String newBranch) throws GitManipulatorException {
        PullResult pullResult;
        try {
            Git git = Git.open(new File(localDir));
            String describe = git.describe().call();
            String activeBranch = this.getActiveBranch(git);
            PullCommand pullCommand = git.pull();
            if (newBranch != null) {
                pullCommand.setRemoteBranchName(newBranch);
            }
            pullResult = pullCommand.call();
            log.info("Active branch: {}", describe, activeBranch);
        } catch (IOException | GitAPIException e) {
            throw new GitManipulatorException(e);
        }
        if(pullResult.isSuccessful()) log.info("isSuccess!");
        log.info("{}", pullResult);
    }

    public List<String> getBranchesByRemoteDir(String remoteDir) throws GitManipulatorException {
        List<String> branches = null;
        try {
            Collection<Ref> refCollection = Git.lsRemoteRepository().setCredentialsProvider(credentialsProvider).setRemote(remoteDir).call();
            branches = new ArrayList<>(refCollection.size());

            for (Ref ref: refCollection) {
                branches.add(ref.getName());
            }

        } catch (GitAPIException e) {
            log.error("Not an git remote dir {}", remoteDir);
            throw new GitManipulatorException("couldn't collect branches", e);
        }
        return branches;
    }

    public List<String> getBranchesByLocalDir(Repository repository) {
        List<String> branches = new ArrayList<>();
        StoredConfig storedConfig = repository.getConfig();

        for (String s1 : storedConfig.getSubsections("branch")) {
            branches.add(s1);
        }

        return branches;
    }

    public List<GitInfo> findSubGit(String dir) throws GitManipulatorException {
        List<GitInfo> gits = new ArrayList<>();
        findAllChildGitProjects(dir, gits);
        gits.forEach((gitInfo) -> log.info("is git directory: {}", gitInfo.getLocalDir()));
        return gits;
    }

    public List<GitInfo> updateSubGit(String dir, String branch) throws GitManipulatorException {
        List<GitInfo> gits = new ArrayList<>();
        findAllChildGitProjects(dir, gits);
        List<Pair<String, Exception>> failed = new ArrayList<>();

        for (GitInfo gitInfo : gits) {
            String currentBranch = gitInfo.getActiveBranch();
            log.debug("Updating: {} to branch: {} ...", gitInfo.getLocalDir(), branch);
            Git git = null;
            try {
                git = Git.open(new File(gitInfo.getLocalDir()));

                if (!branch.equalsIgnoreCase(currentBranch)) {
                    changeBranch(git, branch);
                }

                git.pull().setCredentialsProvider(credentialsProvider).call();
                gitInfo.setActiveBranch(branch);
                log.info("Updated: {} to branch: {}", gitInfo.getLocalDir(), branch);
                git.close();
            } catch (IOException | GitAPIException | GitManipulatorException e) {
                failed.add(new Pair<>("Failed to update: " + gitInfo.getLocalDir() + " to branch: " + branch, e));
                gitInfo.setGitOperationError(e.getMessage());
            } finally {
                if (git != null) git.close();
            }
        }
        failed.forEach(objects -> log.error(objects.getValue0(), objects.getValue1()));
        return gits;
    }


    private void findAllChildGitProjects(String dir, List<GitInfo> gits) throws GitManipulatorException {
        File parentDir = new File(dir);
        if (!parentDir.isDirectory()) {
            return;
        }

        if (ignoreFiles.contains(parentDir.getName())) return;

        Git git;
        try {
            git = Git.open(parentDir);
            GitInfo gitInfo = new GitInfo();
            String remoteUrl = getRemoteDir(git);
            gitInfo.getBranches().addAll(getBranchesByLocalDir(git.getRepository()));
            gitInfo.setActiveBranch(getActiveBranch(git));
            gitInfo.setLocalDir(dir);
            gitInfo.setRemoteUrl(remoteUrl);
            gitInfo.setStatuses(getStatus(git));
            gits.add(gitInfo);
            return;
        } catch (GitAPIException | IOException e) {
            log.error("Not an git dir {}", dir);
        }

        String contents[] = parentDir.list();
        for (int i = 0; i < contents.length; i++) {
            findAllChildGitProjects(parentDir.getAbsolutePath() + System.getProperty("file.separator") + contents[i], gits);
        }
    }

    public String getActiveBranch(Git git) throws GitManipulatorException {
        try {
            return git.getRepository().getBranch();
        } catch (IOException e) {
            throw new GitManipulatorException("Could not find active branch!", e);
        }
    }

    private List<String> getStatus(Git git) throws GitAPIException {
        Status status = git.status().call();
        List<String> changes = new ArrayList<>();
        status.getChanged().forEach(s -> changes.add("changed: " + s));
        status.getModified().forEach(s ->  changes.add("modified: " + s));
        status.getAdded().forEach(s -> changes.add("added: " + s));
        status.getUntracked().forEach(s -> changes.add("untracked: " + s));
        status.getConflicting().forEach(s -> changes.add("conflicting: " + s));
        status.getRemoved().forEach(s -> changes.add("removed: " + s));
        status.getMissing().forEach(s -> changes.add("missing: " + s));
        status.getUncommittedChanges().forEach(s -> changes.add("uncommittedChanges: " + s));
        if (status.isClean()) changes.add("clean");
        return changes;
    }

    private String getRemoteDir(Git git) throws GitAPIException {
        for (RemoteConfig remoteConfig : git.remoteList().call()) {
            for (URIish urIish : remoteConfig.getURIs()) {
                return urIish.toString();
            }
        }

        throw new InvalidRemoteException("Failed to get remote url");
    }
}
