package org.green.kav.cmd.git.exception;

public class GitManipulatorException extends Exception {
    public GitManipulatorException(String message){
        super(message);
    }

    public GitManipulatorException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public GitManipulatorException(Throwable throwable) {
        super(throwable);
    }
}
