package org.green.kav.cmd.git.controller.model;

import java.util.ArrayList;
import java.util.List;

public abstract class GitOperationReportResponseModel {
    private final List<GitFailReport> failed;
    private int failedCount;
    private int successCount;
    private final List<String> successDir;

    public GitOperationReportResponseModel(){
        this.successDir = new ArrayList<>();
        this.failed = new ArrayList<>();
    }

    public void addSuccess(String localDir) {
        this.successDir.add(localDir);
    }

    public void addFailed(String localDir, String errorMessage) {
        this.failed.add(new GitFailReport(localDir, errorMessage));
    }

    public List<GitFailReport> getFailed() {
        return failed;
    }

    public List<String> getSuccessDir() {
        return successDir;
    }

    public int getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(int successCount) {
        this.successCount = successCount;
    }

    public int getFailedCount() {
        return failedCount;
    }

    public void setFailedCount(int failedCount) {
        this.failedCount = failedCount;
    }
}
