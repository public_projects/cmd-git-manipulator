package org.green.kav.cmd.git.controller.interceptor;

import org.springframework.stereotype.Component;

/**
 * TODO: https://sgkcodes.wordpress.com/2016/10/24/handlerinterceptor-to-measure-request-processing-time/
 *
 * To intercept and calculate processing time.
 */
@Component
public class GitRequestInterceptor {}
