package org.green.kav.cmd.git.config;

import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GitManipulatorConfig {

    @Bean
    public CredentialsProvider getCredentialsProvider(GitConfigProperty gitConfigProperty) {
        return new UsernamePasswordCredentialsProvider(gitConfigProperty.getUsername(), gitConfigProperty.getPassword());
    }
}
