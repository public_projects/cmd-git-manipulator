package org.green.kav.cmd.git.controller.model;

import java.util.ArrayList;
import java.util.List;

public class GitCloneRequestModel {
    private String outputDir;
    private List<String> repositories = new ArrayList<>();
    private String branch;

    public String getOutputDir() {
        return outputDir;
    }

    public void setOutputDir(String outputDir) {
        this.outputDir = outputDir;
    }

    public List<String> getRepositories() {
        return repositories;
    }

    public void setRepositories(List<String> repositories) {
        this.repositories = repositories;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    @Override
    public String toString() {
        return "GitCloneRequestModel{" + "outputDir='" + outputDir + '\'' + ", repositories=" + repositories + ", branch='" + branch +
               '\'' + '}';
    }
}
