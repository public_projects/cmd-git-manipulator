package org.green.kav.cmd.git.controller.model;

public class GitFailReport {
    private final String localDir;
    private final String errorMessage;

    public GitFailReport(String localDir, String errorMessage) {
        this.localDir = localDir;
        this.errorMessage = errorMessage;
    }

    public String getLocalDir() {
        return localDir;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}