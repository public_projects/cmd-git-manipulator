package org.green.kav.cmd.git.service.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class GitInfo {
    private String localDir;
    private String remoteUrl;
    private List<String> statuses = new ArrayList<>();
    private String activeBranch;
    private List<String> branches = new ArrayList();
    private String gitOperationError;

    public String getGitOperationError() {
        return gitOperationError;
    }

    public void setGitOperationError(String gitOperationError) {
        this.gitOperationError = gitOperationError;
    }

    public String getActiveBranch() {
        return activeBranch;
    }

    public void setActiveBranch(String activeBranch) {
        this.activeBranch = activeBranch;
    }

    public void setBranches(List<String> branches) {
        this.branches = branches;
    }

    public String getRemoteUrl() {
        return remoteUrl;
    }

    public void setRemoteUrl(String remoteUrl) {
        this.remoteUrl = remoteUrl;
    }

    public List<String> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<String> statuses) {
        this.statuses = statuses;
    }

    public String getLocalDir() {
        return localDir;
    }

    public void setLocalDir(String localDir) {
        this.localDir = localDir;
    }

    public List<String> getBranches() {
        return branches;
    }

    public void addBranch(String branch) {
        this.branches.add(branch);
    }

    @Override
    public int hashCode() {
        return Objects.hash(localDir, remoteUrl, statuses, activeBranch, branches);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GitInfo gitInfo = (GitInfo) o;
        return Objects.equals(localDir, gitInfo.localDir) && Objects.equals(remoteUrl, gitInfo.remoteUrl) && Objects.equals(statuses,
                                                                                                                            gitInfo.statuses) &&
               Objects.equals(activeBranch, gitInfo.activeBranch) && Objects.equals(branches, gitInfo.branches);
    }

    @Override
    public String toString() {
        return "GitInfo{" + "localDir='" + localDir + '\'' + ", remoteUrl='" + remoteUrl + '\'' + ", statuses=" + statuses +
               ", activeBranch='" + activeBranch + '\'' + ", branches=" + branches + '}';
    }
}
