package org.green.kav.cmd.git.controller.model;

import java.util.Objects;

public class GitRequestModel {
    public String parentDirectory;
    public String checkoutBranch;

    public String getParentDirectory() {
        return parentDirectory;
    }

    public void setParentDirectory(String parentDirectory) {
        this.parentDirectory = parentDirectory;
    }

    public String getCheckoutBranch() {
        return checkoutBranch;
    }

    public void setCheckoutBranch(String checkoutBranch) {
        this.checkoutBranch = checkoutBranch;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GitRequestModel that = (GitRequestModel) o;
        return Objects.equals(parentDirectory, that.parentDirectory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(parentDirectory);
    }

    @Override
    public String toString() {
        return "GitRequestModel{" + "parentDirectory='" + parentDirectory + '\'' + '}';
    }
}
