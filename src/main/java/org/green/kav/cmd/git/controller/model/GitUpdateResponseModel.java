package org.green.kav.cmd.git.controller.model;

import org.green.kav.cmd.git.service.model.GitInfo;
import java.util.ArrayList;
import java.util.List;

public class GitUpdateResponseModel extends GitOperationReportResponseModel {

    private final List<GitInfo> gitInfos;

    public GitUpdateResponseModel(){
        super();
        this.gitInfos = new ArrayList<>();
    }

    public void addAllGitInfo(List<GitInfo> gitInfo) {
        this.gitInfos.addAll(gitInfo);
    }

    public List<GitInfo> getGitInfos() {
        return gitInfos;
    }
}


