package org.green.kav.cmd.git;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CmdGitManipulatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(CmdGitManipulatorApplication.class, args);
	}

}
