package org.green.kav.cmd.git.controller.model;

import org.green.kav.cmd.git.service.model.GitInfo;
import java.util.ArrayList;
import java.util.List;

public class GitResponseModel {
    private List<GitInfo> gitInfos = new ArrayList<>();
    public String exception;

    public List<GitInfo> getGitInfos() {
        return gitInfos;
    }

    public void setGitInfos(List<GitInfo> gitInfos) {
        this.gitInfos = gitInfos;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }
}
