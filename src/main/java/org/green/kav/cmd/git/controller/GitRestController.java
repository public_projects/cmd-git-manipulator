package org.green.kav.cmd.git.controller;

import org.green.kav.cmd.git.controller.model.GitCloneRequestModel;
import org.green.kav.cmd.git.controller.model.GitCloneResponseModel;
import org.green.kav.cmd.git.controller.model.GitRequestModel;
import org.green.kav.cmd.git.controller.model.GitResponseModel;
import org.green.kav.cmd.git.controller.model.GitUpdateResponseModel;
import org.green.kav.cmd.git.exception.GitManipulatorException;
import org.green.kav.cmd.git.service.GitManipulatorService;
import org.green.kav.cmd.git.service.model.GitInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerErrorException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
@RequestMapping("/git/v1/api")
public class GitRestController {
    private final Logger log = LoggerFactory.getLogger(GitRestController.class);
    private final static String GET_ALL_URL = "/get/all";
    private final static String UPDATE_ALL_TO_BRANCH_URL = "/update/all/to/branch";
    private final static String CLONE_URL = "/clone";
    private final static String UPDATE_WITH = "/update/with/{branch}";

    private final GitManipulatorService gitManipulatorService;

    @Autowired
    public GitRestController(GitManipulatorService gitManipulatorService){
        this.gitManipulatorService = gitManipulatorService;
    }

    @PostMapping(UPDATE_WITH)
    public @ResponseBody GitResponseModel updateWithBranch(@PathVariable(value="branch") String branch) {
        long start = System.currentTimeMillis();
        log.info("[{}]: request body: {}", UPDATE_WITH, branch);
        return null;
    }

    @PostMapping(CLONE_URL)
    public @ResponseBody GitCloneResponseModel getAllSubGitRemoteUrl(@RequestBody GitCloneRequestModel requestModel) {
        long start = System.currentTimeMillis();
        log.info("[{}]: request body: {}", CLONE_URL, requestModel);
        GitCloneResponseModel responseModel = gitManipulatorService.cloneDir(requestModel.getRepositories(), requestModel.getOutputDir(), requestModel.getBranch());
        log.info("Completed [{}] in {} ms: request body: {}", CLONE_URL, System.currentTimeMillis() - start, responseModel);
        return responseModel;
    }

    @PostMapping(GET_ALL_URL)
    public @ResponseBody GitResponseModel getAllSubGitRemoteUrl(@RequestBody GitRequestModel gitRequestModel) {
        long start = System.currentTimeMillis();
        log.info("[{}]: request body: {}", GET_ALL_URL, gitRequestModel);
        GitResponseModel responseModel = new GitResponseModel();
        try {
            responseModel.setGitInfos(gitManipulatorService.findSubGit(gitRequestModel.getParentDirectory()));
        } catch (GitManipulatorException ex) {
            responseModel.setException(ex.getMessage());
        }
        log.info("Completed [{}] in {} ms: request body: {}", GET_ALL_URL, System.currentTimeMillis() - start, responseModel);
        return responseModel;
    }

    @PostMapping(UPDATE_ALL_TO_BRANCH_URL)
    public @ResponseBody GitUpdateResponseModel updateSubGitBranch(@RequestBody GitRequestModel requestModel) {
        long start = System.currentTimeMillis();
        log.info("[{}]: request body: {}", UPDATE_ALL_TO_BRANCH_URL, requestModel);
        GitUpdateResponseModel responseModel = new GitUpdateResponseModel();
        try {
            constructStatusReport(responseModel, gitManipulatorService.updateSubGit(requestModel.getParentDirectory(),
                                                                                       requestModel.getCheckoutBranch()));
        } catch (GitManipulatorException ex) {
            log.error(ex.getMessage());
            throw new ServerErrorException(ex.getMessage(), ex);
        }
        log.info("Completed [{}] in {} ms: request body: {}", UPDATE_ALL_TO_BRANCH_URL, System.currentTimeMillis() - start,requestModel);
        return responseModel;
    }

    private void constructStatusReport(GitUpdateResponseModel responseModel, List<GitInfo> updateSubGit) {
        AtomicInteger successes = new AtomicInteger();
        AtomicInteger fails = new AtomicInteger();
        responseModel.addAllGitInfo(updateSubGit);
        for (GitInfo gitInfo: updateSubGit) {
            if (gitInfo.getGitOperationError() == null || gitInfo.getGitOperationError().isBlank()) {
                responseModel.addSuccess(gitInfo.getLocalDir());
                successes.incrementAndGet();
                continue;
            }

            fails.incrementAndGet();
            responseModel.addFailed(gitInfo.getLocalDir(), gitInfo.getGitOperationError());
        }

        responseModel.setFailedCount(fails.get());
        responseModel.setSuccessCount(successes.get());
    }
}
